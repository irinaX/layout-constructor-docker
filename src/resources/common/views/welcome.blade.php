<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Base App</title>
    <link href="{{ asset('static/common/css/app.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
</head>
<body>
<div id="app"></div>
<script src="{{ asset('/static/common/js/app.js') }}"></script>
</body>
</html>
