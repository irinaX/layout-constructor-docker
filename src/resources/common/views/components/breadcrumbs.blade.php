<section class="container">
    <ol class="breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        @foreach($items as $name=>$url)
            @if(!$loop->last)
                <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" title="{{$name}}" href="{{$url}}">
                        <span itemprop="name">{{$name}}</span>
                        <meta itemprop="position" content="{{$loop->index+1}}">
                    </a>
                </li>
            @else
                <li title="{{$name}}">
                    <span>{{$name}}</span>
                </li>
            @endif
        @endforeach
    </ol>
</section>
