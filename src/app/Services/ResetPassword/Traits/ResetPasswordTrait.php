<?php


namespace App\Services\ResetPassword\Traits;


trait ResetPasswordTrait
{
    public function getPasswordField(): string
    {
        return 'password';
    }
}
