<?php

namespace App\Services\ResetPassword\Interfaces;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin  Model
 */
interface ResetPasswordInterface
{
    public function getPasswordField(): string;
}
