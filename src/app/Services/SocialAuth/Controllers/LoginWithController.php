<?php


namespace App\Services\SocialAuth\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class LoginWithController
{
    public function loginWith(Request $request, string $provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleLoginWithCallback(Request $request, string $provider)
    {
        $userProvided = null;
        try {
            $userProvided = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect('/');
        }

        $user = User::where('email', $userProvided->getEmail())->first();
        if (!$user) {
            $splitName = explode(' ', $userProvided->getName(), 2);
            $user = new User([
                'first_name' => $splitName[0],
                'last_name' => $splitName[1],
                'email' => $userProvided->getEmail(),
                'password' => $userProvided->token,
            ]);
            $user->save();
        }
        Auth::login($user);
        return redirect('/');
    }
}
