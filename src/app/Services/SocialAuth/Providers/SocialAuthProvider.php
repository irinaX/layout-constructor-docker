<?php

namespace App\Services\SocialAuth\Providers;

use App\Services\SocialAuth\Controllers\LoginWithController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class SocialAuthProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::group(['middleware' => 'web'], function () {
            Route::get('login-with/{provider}', LoginWithController::class . '@loginWith');
            Route::get('login-with/callbacks/{provider}', LoginWithController::class . '@handleLoginWithCallback');
        });
    }
}
