<?php

namespace App\Services\PhoneVerification\Interfaces;

interface PhoneVerificationInterface
{
    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedPhone();

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markPhoneAsVerified();


    /**
     * Get the email address that should be used for verification.
     *
     * @return string
     */
    public function getPhoneForVerification();
}
