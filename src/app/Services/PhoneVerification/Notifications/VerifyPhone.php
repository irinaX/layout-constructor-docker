<?php /** @noinspection PhpUnused */

namespace App\Services\PhoneVerification\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\SmscRu\SmscRuChannel;
use NotificationChannels\SmscRu\SmscRuMessage;

class VerifyPhone extends Notification
{
    private string $verificationCode;

    public function __construct(string $code)
    {
        $this->verificationCode = $code;
    }

    /**
     * Get the notification's channels.
     *
     * @return array|string
     */
    public function via()
    {
        return [
            SmscRuChannel::class,
        ];
    }


    public function toText()
    {
        return "Код подтверждения номера телефона на сайте razvivaites.ru: " . $this->verificationCode;
    }


    public function toSmscRu($notifiable)
    {
        return SmscRuMessage::create($this->toText());
    }
}
