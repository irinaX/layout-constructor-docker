<?php


namespace App\Services\PhoneVerification\Services;

use App\Services\PhoneVerification\Interfaces\PhoneVerificationInterface;
use App\Services\PhoneVerification\Notifications\VerifyPhone;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class PhoneVerificationService
{
    private string $cacheKey;
    /**
     * @var PhoneVerificationInterface
     */
    private PhoneVerificationInterface $notifiable;

    public function __construct(PhoneVerificationInterface $notifiable)
    {
        $this->notifiable = $notifiable;
        $this->cacheKey = md5('phone_verification' . get_class($this->notifiable) . $this->notifiable->getKey());
    }

    public function sendVerificationNotification()
    {
        $code = rand(100000, 999999);
        Cache::put($this->cacheKey, $code, 300);
        Notification::send($this->notifiable, new VerifyPhone($code));
    }

    public function verify($code)
    {
        if ($this->notifiable->hasVerifiedPhone())
            throw ValidationException::withMessages(['code' => 'Телефон уже подтверждён.']);
        else if (Cache::get($this->cacheKey) != $code)
            throw ValidationException::withMessages(['code' => 'Неверный код подтверждения.']);
        return true;
    }
}
