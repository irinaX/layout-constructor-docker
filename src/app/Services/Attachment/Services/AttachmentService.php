<?php


namespace App\Services\Attachment\Services;


use App\Services\Attachment\Interfaces\AttachmentInterface;
use App\Services\Attachment\Models\Attachment;
use App\Services\Attachment\Resources\AttachmentResource;
use Spatie\ImageOptimizer\OptimizerChain;

class AttachmentService implements AttachmentInterface
{

    public function store($file, $type)
    {
        $createdFile = null;
        if ($type == 'embed')
            $createdFile = $this->storeIframe($file);
        else {
            $createdFile = $this->storeFile($file);
        }
        return new AttachmentResource($createdFile);
    }

    private function storeFile($file)
    {
        $path = 'upload';
        $disk = config('attachments.disk', 'public');
        app(OptimizerChain::class)->optimize($file);
        $storedFile = $file->store($path, $disk);

        return Attachment::create([
            'name' => $file->getClientOriginalName(),
            'type' => $file->getMimeType(),
            'size' => $file->getSize(),
            'original' => $storedFile,
            'disk' => $disk,
//            'thumbnail' => $thumbnailUrl ?? null,
        ]);
    }

    private function storeIframe($file)
    {
        return Attachment::create([
            'name' => 'Внешнее содержимое',
            'type' => 'embed',
            'original' => $file,
            'thumbnail' => null,
        ]);
    }
}
