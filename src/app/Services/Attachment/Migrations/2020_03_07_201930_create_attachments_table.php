<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('disk')->default('public');
            $table->string('original')->nullable();
            $table->string('thumbnail')->nullable();
            $table->json('info')->nullable();
            $table->string('type');
            $table->string('alt')->nullable();
            $table->unsignedBigInteger('size')->nullable();
            $table->unsignedInteger('order_column')->nullable();
            $table->timestamps();

            $table->nullableMorphs('attachable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
