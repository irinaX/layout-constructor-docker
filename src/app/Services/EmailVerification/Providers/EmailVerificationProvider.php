<?php

namespace App\Services\EmailVerification\Providers;

use App\Services\EmailVerification\Interfaces\EmailVerificationInterface;
use App\Services\EmailVerification\Services\EmailVerificationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class EmailVerificationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmailVerificationService::class);
        $this->app->when(EmailVerificationService::class)
            ->needs(EmailVerificationInterface::class)
            ->give(function () {
                return Auth::user();
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $controllerNamespace = 'App\Services\EmailVerification\Controllers';
        Route::group([
            'prefix' => 'api/user/verification',
            'middleware' => 'api',
            'namespace' => $controllerNamespace
        ], function () {
            Route::get('email/status', 'EmailVerificationController@status')->name('email-verification.status');
            Route::get('email', 'EmailVerificationController@send')->name('email-verification.send');
            Route::post('email', 'EmailVerificationController@verifyCode')->name('email-verification.verify-code');
        });

        Route::group([
            'middleware' => 'web',
            'namespace' => $controllerNamespace,
        ], function () {
            Route::get('email/verify', 'EmailVerificationController@verifyRedirect')
                ->name('email-verification.verify-redirect');
        });
    }
}
